class Match(object):
    def __init__(self):
        self.players = [];

    # TODO: Complete Current Model
    # matchType - init data, derived
    # mapName - details
    # matchLength - not sure
    # heroName - details
    # heroLevel - not sure
    # matchmakingRating - calculated
    # ratingAdjustmentPoints - calculated
    # matchDateTime - details (see TODO below)
    # TODO: Figure out how these fields translate to datetime { 'm_timeLocalOffset': -180000000000L, 'm_timeUTC': 131416855219612619L }

    # TODO: Changes for Future Model
    # matchOutcome - details or init data
    # talent choices - tracker events
    # score result screen stats - tracker events
    # teammates and enemy team player names and hero picks - details
    # season - derived
    # playerName - details